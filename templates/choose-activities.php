<h3><?php _e( 'Choose actvities to import', 'StravaPost' ); ?></h3>

<style>
	.strava-activity { width: 50%; }
	.ui-selected { background: #F39814; color: white; }
</style>
<p>
	<?php printf( _n( 'Found %d activity.', 'Found %d activities.', count( $activities ) ), count( $activities ) ); ?>
	<?php if ( isset( $view['page'] ) ) : ?>
		<?php if ( $view['page'] > 1 ) : ?>
			<a href="<?php echo add_query_arg( array(
				'view[page]' => $view['page'] - 1,
				'view[per_page]' => $view['per_page'],
			) ); ?>"><?php _e( 'Newer Activities', 'StravaPost' ); ?></a>
		<?php endif; ?>
		<?php if ( $view['per_page'] == count( $activities ) ) : ?>
			<a href="<?php echo add_query_arg( array(
				'view[page]' => $view['page'] + 1,
				'view[per_page]' => $view['per_page'],
			) ); ?>"><?php _e( 'Older Activities', 'StravaPost' ); ?></a>
		<?php endif; ?>
	<?php endif; ?>
</p>
<table class="wp-list-table widefat fixed">
	<thead>
		<tr>
			<th scope="col" class="check-column">
				<label class="screen-reader-text" for="cb-select-all-1">Select All</label>
				<input id="cb-select-all-1" class="activity-cb" type="checkbox" />
			</th>
			<th>Name</th>
			<th>Date</th>
			<th>Type</th>
			<th>Location</th>
			<th>Imported</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th scope="col" class="check-column">
				<label class="screen-reader-text" for="cb-select-all-2">Select All</label>
				<input id="cb-select-all-2" class="activity-cb" type="checkbox" />
			</th>
			<th>Name</th>
			<th>Date</th>
			<th>Type</th>
			<th>Location</th>
			<th>Imported</th>
		</tr>
	</tfoot>
	<tbody id="strava-activities" data-nonce="<?php echo wp_create_nonce(); ?>">
		<?php foreach( $activities as $activity ) : ?>
			<?php $post = \StravaPost\Activity::get_by_activity_id( $activity->id ); ?>
			<tr data-activity-id="<?php echo $activity->id; ?>" class="<?php echo $post ? ' ui-disabled' : 'strava-activity'; ?>">
				<th scope="row" class="check-column">
					<?php if ( !$post ) : ?>
						<label class="screen-reader-text" for="cb-select-<?php echo $activity->id; ?>">
							Select <?php echo $activity->name; ?>
						</label>
						<input type="checkbox"
						       name="activities[]"
						       class="activity-cb"
						       id="activity-<?php echo $activity->id; ?>"
						       value="<?php echo $activity->id; ?>" />
					<?php endif; ?>
				</th>
				<td><?php echo $activity->name; ?></td>
				<td><?php echo date( 'Y-m-d g:i a', strtotime( $activity->start_date ) ); ?></td>
				<td><?php echo $activity->type; ?></td>
				<td><?php echo $activity->location_city; ?>, <?php echo $activity->location_state; ?></td>
				<td class="activity-imported">
					<?php if( $post ) : ?>
						<?php edit_post_link( __( 'Yes' ), '', '', $post->ID ); ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<div class="submit">
	<input id="import-activities-submit" class="button button-primary" type="submit" value="Create Posts" disabled="disabled" />
</div>

<a href="<?php echo add_query_arg( array( 'step' => 'athlete-token-step.php' ) ); ?>">
	Change Athlete
</a>
