<?php if ( !empty( $error ) ) : ?>
	<div class="error">
		<p><?php echo $error; ?></p>
	</div>
<?php endif; ?>

<h3><?php _e( 'Authorize an Athlete', 'StravaPost' ); ?></h3>

<form method="post">
	<label><?php _e( 'Athlete Token', 'StravaPost' ); ?></label>
	<input name="athlete_token" type="text" value="<?php echo \StravaPost\Options::get( 'athlete_token' ); ?>" />
	<input name="strava_post_action" type="hidden" value="save_athlete_token" />
	<div class="submit">
		<input class="button button-primary" type="submit" name="submit" />
	</div>
</form>
