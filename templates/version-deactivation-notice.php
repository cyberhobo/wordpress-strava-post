<div class="error">
	<p>
		<?php 
		printf( 
			__( 'StravaPost will deactivate itself because it requires a minimum PHP version of %1$s, and you have %2$s installed. Contact your web host to upgrade.' ),
			STRAVAPOST_PHP_MINIMUM_VERSION, 
			PHP_VERSION
		);
		?>
	</p>
</div>
