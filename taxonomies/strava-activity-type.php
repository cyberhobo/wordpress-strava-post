<?php

/**
 * Want to override something in here? Don't change it here, this file is overwritten by upgrades!
 * Intead, copy this file to your theme directory, require it from your functions.php file, and make the last line:
 *
 * add_action( 'init', 'strava_activity_type_init', 9 );
 */
function strava_activity_type_init() {

	if ( !taxonomy_exists( 'strava-activity-type' ) ) {

		register_taxonomy( 'strava-activity-type', array( 'strava-post' ), array(
			'hierarchical'      => false,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_ui'           => true,
			'show_admin_column' => false,
			'query_var'         => true,
			'rewrite'           => true,
			'capabilities'      => array(
				'manage_terms'  => 'edit_posts',
				'edit_terms'    => 'edit_posts',
				'delete_terms'  => 'edit_posts',
				'assign_terms'  => 'edit_posts'
			),
			'labels'            => array(
				'name'                       => __( 'Activity Types', 'StravaPost' ),
				'singular_name'              => _x( 'Activity Type', 'taxonomy general name', 'StravaPost' ),
				'search_items'               => __( 'Search Activity Types', 'StravaPost' ),
				'popular_items'              => __( 'Popular Activity Types', 'StravaPost' ),
				'all_items'                  => __( 'All Activity Types', 'StravaPost' ),
				'parent_item'                => __( 'Parent Activity Type', 'StravaPost' ),
				'parent_item_colon'          => __( 'Parent Activity Type:', 'StravaPost' ),
				'edit_item'                  => __( 'Edit Activity Type', 'StravaPost' ),
				'update_item'                => __( 'Update Activity Type', 'StravaPost' ),
				'add_new_item'               => __( 'New Activity Type', 'StravaPost' ),
				'new_item_name'              => __( 'New Activity Type', 'StravaPost' ),
				'separate_items_with_commas' => __( 'Activity Types separated by comma', 'StravaPost' ),
				'add_or_remove_items'        => __( 'Add or remove Activity Types', 'StravaPost' ),
				'choose_from_most_used'      => __( 'Choose from the most used Activity Types', 'StravaPost' ),
				'menu_name'                  => __( 'Activity Types', 'StravaPost' ),
			),
		) );
	}
}

add_action( 'init', 'strava_activity_type_init' );