<?php

namespace StravaPost;

class Tool_Page {

	static protected $athlete_token;

	static public function head() {
		self::process_form_data();

		wp_enqueue_style(
			'jquery-smoothness',
			'//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css',
			array(),
			'1.10.4'
		);
		wp_enqueue_script( 'jquery-ui-selectable' );
		wp_enqueue_script(
			'strava-post-choose-activities',
			Core::get_url( 'js/choose-activities.js' ),
			array( 'jquery-ui-selectable' ),
			Core::VERSION,
			true
		);
	}

	static public function content() {
		$step = array_key_exists( 'step', $_GET ) ? $_GET['step'] : null;
		if ( !$step ) {
			self::$athlete_token = Options::get( 'athlete_token' );

			if ( !self::$athlete_token )
				$step = 'athlete-token-step.php';
			else {
				$default_view = array(
					'page' => 1,
					'per_page' => 10,
				);
				$view = empty( $_GET['view'] ) ? $default_view : $_GET['view'];
				$activities = Strava_API::get_activities( $view );

				if ( is_array( $activities ) ) {
					$step = 'choose-activities.php';
				} else {
					if ( is_wp_error( $activities ) )
						$error = $activities->get_error_message();
					else
						$error = $activities->message;

					$step = 'athlete-token-step.php';
				}
			}
		}

		include path_join( Core::get_dir( 'templates' ), $step );

	}

	static protected function process_form_data() {
		if ( !array_key_exists( 'strava_post_action', $_POST ) )
			return;

		if ( isset( $_POST['athlete_token'] ) )
			Options::set( 'athlete_token', sanitize_key( $_POST['athlete_token'] ) );
	}

}