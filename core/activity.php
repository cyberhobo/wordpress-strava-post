<?php

namespace StravaPost;

class Activity {

	protected $_id;
	protected $_post;

	static public function get_meta_key( $activity_field, $activity = null ) {
		return apply_filters( 'strava_post_activity_meta_key', $activity_field, $activity );
	}

	static public function get_by_activity_id( $activity_id ) {
		$posts = get_posts( array(
			'post_type' => 'any',
			'meta_key' => self::get_meta_key( 'id' ),
			'meta_value' => intval( $activity_id ),
			'posts_per_page' => 1,
		) );

		if ( empty( $posts ) )
			return null;

		return new Activity( $posts[0] );
	}

	static public function ajax_import_new() {
		if ( !wp_verify_nonce( $_POST['nonce'] ) )
			wp_die( -1 );

		if ( !current_user_can( 'import' ) )
			wp_die( -1 );

		$activity = self::import_new( $_POST['activity_id'] );
		if ( is_wp_error( $activity ) ) {
			echo json_encode( array(
				'status' => 'error',
				'message' => $activity->get_error_message()
			) );
		} else {
			echo json_encode( array(
				'status' => 'ok',
				'id' => $activity->get_id(),
				'edit_url' => get_edit_post_link( $activity->get_id() ),
			) );
		}
		wp_die();
	}

	static public function import_new( $activity_id ) {
		if ( self::get_by_activity_id( $activity_id ) )
			return new \WP_Error( 'strava_post_exists', __( 'A post has already been imported for that activity', 'StravaPost' ) );

		$activity = Strava_API::get_activity( $activity_id );

		if ( is_wp_error( $activity ) )
			return $activity;

		$width = get_option( 'thumbnail_size_w' );
		$height = get_option( 'thumbnail_size_h' );
		$map = '<a href="http://www.strava.com/activities/' . $activity_id .
			'"><img class="alignleft" src="http://maps.googleapis.com/maps/api/staticmap?sensor=false&maptype=terrain&size=' .
			$width . 'x' . $height . '&path=weight:3%7Ccolor:red%7Cenc:' .
			wp_slash( $activity->map->summary_polyline ) . '" /></a>';

		$post_data = array(
			'post_type' => Core::POST_TYPE,
			'post_status' => 'publish',
			'post_title' => $activity->name,
			'post_content' => $map . $activity->description,
			'post_date' => $activity->start_date_local,
			'post_date_gmt' => $activity->start_date,
			'post_author' => get_current_user_id(),
		);
		$post_id = wp_insert_post( $post_data, true );

		if ( is_wp_error( $post_id ) )
			return $post_id;

		$our_activity = new Activity( $post_id );
		$skip_fields = array( 'name', 'description', 'start_date' );
		foreach ( (array)$activity as $name => $value ) {
			if ( !in_array( $name, $skip_fields ) )
				$our_activity->set( $name, $value );
		}
		do_action( 'strava_post_imported_activity', $our_activity, $activity );

		return $our_activity;
	}

	public function __construct( $post_or_id ) {
		if ( is_a( $post_or_id, 'WP_Post' ) ) {
			$this->_id = $post_or_id->ID;
			$this->_post = $post_or_id;
		} else {
			$this->_id = intval( $post_or_id );
		}
	}

	public function __get( $name ) {
		$method = 'get_' . $name;
		if ( method_exists( $this, $method ) )
			return $this->$method();
		$meta_key = self::get_meta_key( $name, $this );
		return get_post_meta( $this->_id, $meta_key, true );
	}

	public function get_id() {
		return $this->_id;
	}

	public function get_post() {
		if ( ! isset( $this->_post ) )
			$this->_post = get_post( $this->_id );
		return $this->_post;
	}

	protected function set( $name, $value = null ) {
		$meta_key = self::get_meta_key( $name, $this );
		return update_post_meta( $this->_id, $meta_key, $value );
	}
}
