<?php

namespace StravaPost;

class Options {

	static protected $options = null;

	static protected $defaults = array(
		'athlete_token' => null,
	);

	static protected function ensure_loaded() {
		if ( !self::$options )
			self::$options = array_merge( self::$defaults, get_option( 'strava_post_options', array() ) );
	}

	static public function get( $field, $default = null ) {
		self::ensure_loaded();
		return isset( self::$options[$field] ) ? self::$options[$field] : $default;
	}

	static public function set( $field, $value = '' ) {
		self::ensure_loaded();
		if ( array_key_exists( $field, self::$defaults ) ) {
			self::$options[$field] = $value;
			update_option( 'strava_post_options', self::$options );
		}
	}
}
