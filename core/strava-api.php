<?php

namespace StravaPost;

class Strava_API {

	static public function url( $endpoint = '', $query = array() ) {
		$url = 'https://www.strava.com/api/v3';
		if ( $endpoint )
			$url = path_join( $url, $endpoint );
		if ( !empty( $query ) )
			$url .= '/?' . http_build_query( $query );
		return $url;
	}

	static public function get_json( $endpoint = '', $query = array() ) {
		$response = wp_remote_get( self::url( $endpoint, $query ) );
		if ( is_wp_error( $response ) )
			return $response;

		if ( '200' != $response['response']['code'] )
			return new \WP_Error( 'strava_get_failure', $response['response']['code'] . ' ' . $response['response']['message'] );

		return $response['body'];
	}

	static public function get( $endpoint = '', $query = array() ) {
		$json = self::get_json( $endpoint, $query );
		if ( is_wp_error( $json ) )
			return $json;
		$data = json_decode( $json );
		if ( is_null( $data ) )
			return new \WP_Error( 'strava_no_data', __( 'The Strava API may be down or not replying normally.', 'StravaPost' ) );
		return $data;
	}

	static public function get_activities( $query = array() ) {
		$query = self::ensure_access_token( $query );
		$activities = self::get( 'athlete/activities', $query );
		if ( is_wp_error( $activities ) )
			return $activities;
		if ( empty( $activities ) )
			$activities = array();
		return $activities;
	}

	static public function get_activity( $id, $query = array() ) {
		$activity = get_transient( self::activity_transient_key( $id ) );

		if ( !$activity ) {
			$query = self::ensure_access_token( $query );
			$activity = self::get( "activities/$id", $query );
			set_transient( self::activity_transient_key( $id ), $activity, 24*60*60 );
		}

		return $activity;
	}

	static protected function ensure_access_token( $query ) {
		if ( empty( $query['access_token'] ) ) {
			$query['access_token'] = Options::get( 'athlete_token' );
		}
		return $query;
	}

	static protected function activity_transient_key( $id ) {
		return 'strava_post_activity_' . $id;
	}
}