<?php

namespace StravaPost;

class Core {

	const VERSION = '0.1-alpha';
	const POST_TYPE = 'strava-post';
	const ACTIVITY_TYPE_TAXONOMY = 'strava-activity-type';

	static protected $base_dir;
	static protected $base_url;

	static public function load() {

		self::$base_dir = dirname( dirname( __FILE__ ) );
		self::$base_url = plugins_url( '', dirname( __FILE__ ) );

		load_plugin_textdomain( 'StravaPost', '', basename( self::$base_dir ) . '/lang' );

		add_action( 'admin_menu', array( '\\StravaPost\\Admin', 'add_management_page' ) );

		add_action( 'wp_ajax_strava_post_import_activity', array( '\\StravaPost\\Activity', 'ajax_import_new' ) );

		require_once self::$base_dir . '/post-types/strava-post.php';
		require_once self::$base_dir . '/taxonomies/strava-activity-type.php';
	}

	static public function get_dir( $path = '' ) {
		return path_join( self::$base_dir, $path );
	}

	static public function get_url( $path = '' ) {
		return path_join( self::$base_url, $path );
	}

}