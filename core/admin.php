<?php

namespace StravaPost;

class Admin {

	const TOOL_MENU_SLUG = 'strava-post-tool';

	static public function add_management_page() {

		$tool_page_hook = add_management_page(
			__( 'Strava Post', 'StravaPost' ),
			__( 'Strava Post', 'StravaPost' ),
			'import',
			self::TOOL_MENU_SLUG,
			array( '\\StravaPost\\Tool_Page', 'content' )
		);

		add_action( 'admin_print_styles-' . $tool_page_hook, array( '\\StravaPost\\Tool_Page', 'head' ) );

	}

}