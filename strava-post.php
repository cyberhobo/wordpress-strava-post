<?php
/*
Plugin Name: Strava Post
Version: 0.1-alpha
Description: Publish your Strava activities as posts.
Author: Dylan Kuhn
Author URI: http://www.cyberhobo.net/
Plugin URI: http://www.cyberhobo.net/
Text Domain: strava-post
Domain Path: /languages
*/

define( 'STRAVAPOST_PHP_MINIMUM_VERSION', '5.3' );

if ( version_compare( PHP_VERSION, STRAVAPOST_PHP_MINIMUM_VERSION, '>=' ) ) {

	// PHP 5.2 parseable code to load our 5.3 app
	require_once dirname( __FILE__ ) . '/core/autoload.php';
	call_user_func( array( '\\StravaPost\\Autoload', 'register' ) );
	call_user_func( array( '\\StravaPost\\Core', 'load' ) );

} else {

	// We don't have PHP 5.3, fail nicely
	add_action( 'admin_notices', 'stravapost_version_deactivation_notice' );

}

function stravapost_version_deactivation_notice() {

	include dirname( __FILE__ ) . '/templates/version-deactivation-notice.php';
	deactivate_plugins( plugin_basename( __FILE__ ) );

}

