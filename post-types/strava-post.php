<?php

/**
 * Want to override something in here? Don't change it here, this file is overwritten by upgrades!
 * Intead, copy this file to your theme directory, require it from your functions.php file, and make the last line:
 *
 * add_action( 'init', 'strava_post_init', 9 );
 */

function strava_post_init() {
	if ( !post_type_exists( 'strava-post' ) ) {

		add_filter( 'post_updated_messages', 'strava_post_updated_messages' );

		register_post_type( 'strava-post', array(
			'hierarchical'      => false,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_ui'           => true,
			'supports'          => array( 'title', 'editor', 'custom-fields' ),
			'has_archive'       => true,
			'query_var'         => true,
			'rewrite'           => true,
			'labels'            => array(
				'name'                => __( 'Strava Posts', 'StravaPost' ),
				'singular_name'       => __( 'Strava Post', 'StravaPost' ),
				'all_items'           => __( 'Strava Posts', 'StravaPost' ),
				'new_item'            => __( 'New Strava Post', 'StravaPost' ),
				'add_new'             => __( 'Add New', 'StravaPost' ),
				'add_new_item'        => __( 'Add New Strava Post', 'StravaPost' ),
				'edit_item'           => __( 'Edit Strava Post', 'StravaPost' ),
				'view_item'           => __( 'View Strava Post', 'StravaPost' ),
				'search_items'        => __( 'Search Strava Posts', 'StravaPost' ),
				'not_found'           => __( 'No Strava Posts found', 'StravaPost' ),
				'not_found_in_trash'  => __( 'No Strava Posts found in trash', 'StravaPost' ),
				'parent_item_colon'   => __( 'Parent Strava Post', 'StravaPost' ),
				'menu_name'           => __( 'Strava Posts', 'StravaPost' ),
			),
		) );

	}
}

function strava_post_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['strava-post'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Strava Post updated. <a target="_blank" href="%s">View Strava Post</a>', 'StravaPost'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'StravaPost'),
		3 => __('Custom field deleted.', 'StravaPost'),
		4 => __('Strava Post updated.', 'StravaPost'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Strava Post restored to revision from %s', 'StravaPost'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Strava Post published. <a href="%s">View Strava Post</a>', 'StravaPost'), esc_url( $permalink ) ),
		7 => __('Strava Post saved.', 'StravaPost'),
		8 => sprintf( __('Strava Post submitted. <a target="_blank" href="%s">Preview Strava Post</a>', 'StravaPost'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Strava Post scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Strava Post</a>', 'StravaPost'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Strava Post draft updated. <a target="_blank" href="%s">Preview Strava Post</a>', 'StravaPost'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}

add_action( 'init', 'strava_post_init' );
