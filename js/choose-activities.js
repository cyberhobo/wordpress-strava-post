jQuery( function( $ ) {
	var $checkboxes = $( 'input.activity-cb' ),
		$submit = $( '#import-activities-submit' ),
		$tbody = $( '#strava-activities' ),
		nonce = $tbody.data( 'nonce' );

	$checkboxes.click( function( e ) {
		var $checked = $checkboxes.filter( ':checked' );
		if ( $checked.length > 0 ) {
			$submit.prop( 'disabled', false );
		} else {
			$submit.prop( 'disabled', true );
		}
	} );

	$submit.click( function( e ) {
		var $checked = $checkboxes.filter( ':checked' );
		e.preventDefault();
		if ( !$checked.length ) {
			return;
		}
		$submit.prop( 'disabled', true );
		$checked.prop( 'disabled', true ).each( function( i, checkbox ) {
			var $cb = $( checkbox ),
				$row = $cb.parent().parent( 'tr' ),
				$imported_col = $row.find( '.activity-imported' ),
				activity_id = $cb.val();

			$imported_col.html( '<span class="spinner" style="display: inline;"></span>' );

			$.ajax( {
				url: ajaxurl,
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'strava_post_import_activity',
					nonce: nonce,
					activity_id: activity_id
				},
				success: function( data ) {
					if ( data.status === 'ok' ) {
						$cb.detach();
						$imported_col.empty().append(
							$( '<a></a>' ).attr( 'href', data.edit_url ).text( data.id )
						);
					} else {
						$cb.prop( 'disabled', false );
						$imported_col.html( data.message );
					}
				},
				error: function( jqXHR, status ) {
					$imported_col.html( status );
				}
			} );

		} );
	})
} );